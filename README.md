```
$ mkdir xstate-learning
$ cd xstate-learning
$ git init
$ npm init -y
$ npm install xstate --save
```

I then created [machine.js](machine.js) and edited [package.json](package.json) file so that the ```main``` property pointed to ```machine.js```
```javascript
"main": "machine.js",
```

and to add the ```start``` property to the ```scripts``` property:

```javascript
"scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "node machine.js"
  },
  ```

  In ```xstate-learning``` running:
  ```
  $ npm start
  ```

  results in:
  ```
  > xstate-learning@1.0.0 start /home/steven/repositories/xstate-learning
> node machine.js

pending
resolved

  ```